@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
					             <h4>Edit Item</h4>
                </div>
                <p class="text-success">{{Session::get('message')}}</p>
                <div class="card-body">
                  <form method="post" action="{{route('updateItem')}}">
                    @csrf
                    <div class="form-group">
                      <label for="exampleInputEmail1">Name</label>
                      <input type="text" name="name" class="form-control" value="{{$singleItem->name}}"  placeholder="Enter Name">
                      <input type="hidden" name="id" class="form-control" value="{{$singleItem->id}}"  placeholder="Enter Name">

                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Type</label>
                      <input type="text" name="type" class="form-control" value="{{$singleItem->type}}"  placeholder="Type">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Qty</label>
                      <input type="number" name="qty" class="form-control" value="{{$singleItem->qty}}"  placeholder="Type">
                    </div>

                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>





<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Creat new item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

    </div>
  </div>
</div>
@endsection
